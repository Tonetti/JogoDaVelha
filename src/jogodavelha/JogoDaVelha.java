
package jogodavelha;

import br.com.jogodavelha.view.JogadoresView;
import br.com.jogodavelha.view.TabuleiroView;

/**
 *
 * @author moises
 */
public class JogoDaVelha {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new JogadoresView().setVisible(true);
    }
    
}
