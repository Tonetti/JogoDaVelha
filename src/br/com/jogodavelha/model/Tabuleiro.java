package br.com.jogodavelha.model;

public class Tabuleiro {

    private char matriz[][] = new char[3][3];

    public Tabuleiro() {
        resetaMatriz();
    }

    public char[][] getMatriz() {
        return matriz;
    }

    public void setMatriz(char[][] matriz) {
        this.matriz = matriz;
    }

    public void resetaMatriz() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                matriz[i][j] = '-';
            }
        }
    }

    public void marcaNaMatriz(char simbolo, int linha, int coluna) {
        matriz[linha][coluna] = simbolo;

    }

    public void mostraMatriz() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(matriz[i][j]);
            }
            System.out.println();
        }

    }

    public boolean verSeSimboloGanhou(char simbolo) {
        int contador_l = 0, contador_c = 0, contador_de = 0, contador_dd = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (simbolo == matriz[i][j]) {
                    contador_l++;
                }
                if (simbolo == matriz[j][i]) {
                    contador_c++;
                }
            }
            if (simbolo == matriz[i][i]) {
                contador_de++;             
            }
            if (simbolo == matriz[i][2 - i]) {
                contador_dd++;
            }
            if (contador_c == 3 || contador_l == 3 || contador_de == 3 || contador_dd == 3) {
                return true;
            }
            contador_l = 0;
            contador_c = 0;
        }   
        return false;
    }

}
