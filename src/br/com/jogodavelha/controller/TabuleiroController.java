/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jogodavelha.controller;

import br.com.jogodavelha.model.Jogador;
import br.com.jogodavelha.model.Tabuleiro;

public class TabuleiroController {

    private Tabuleiro tabuleiroModel;
    private Jogador jogador1;
    private Jogador jogador2;
    private Jogador jogadorAtual;
    private int quantJogadas = 0;

    public TabuleiroController() {
        this.tabuleiroModel = new Tabuleiro();
    }

    public boolean estaVazio(int linha, int coluna) {
        if (this.tabuleiroModel.getMatriz()[linha][coluna] == '-') {
            return true;
        } else {
            return false;

        }
    }

    public int Jogar(int linha, int coluna) {
        quantJogadas++;
        this.tabuleiroModel.marcaNaMatriz(jogadorAtual.getSimbolo(), linha, coluna);
        this.tabuleiroModel.mostraMatriz();
        if (tabuleiroModel.verSeSimboloGanhou(jogadorAtual.getSimbolo())) {
            this.jogadorAtual.setVitorias(this.jogadorAtual.getVitorias() + 1);
            this.tabuleiroModel.resetaMatriz();
            quantJogadas = 0;
            return 2;

        } else if (quantJogadas == 9) {
            this.tabuleiroModel.resetaMatriz();
            quantJogadas = 0;
            return 1;
        } else {

            if (this.jogadorAtual.equals(this.jogador1)) {
                this.jogadorAtual = jogador2;
            } else if (this.jogadorAtual.equals(this.jogador2)) {
                this.jogadorAtual = jogador1;
            }

            return 0;
        }
    }

    public Tabuleiro getTabuleiroModel() {
        return tabuleiroModel;
    }

    public void setTabuleiroModel(Tabuleiro tabuleiroModel) {
        this.tabuleiroModel = tabuleiroModel;
    }

    public Jogador getJogador1() {
        return jogador1;
    }

    public void setJogador1(Jogador jogador1) {
        this.jogador1 = jogador1;
    }

    public Jogador getJogador2() {
        return jogador2;
    }

    public void setJogador2(Jogador jogador2) {
        this.jogador2 = jogador2;
    }

    public Jogador getJogadorAtual() {
        return jogadorAtual;
    }

    public void setJogadorAtual(Jogador jogadorAtual) {
        this.jogadorAtual = jogadorAtual;
    }

}
