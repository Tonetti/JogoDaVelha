package br.com.jogodavelha.controller;

import br.com.jogodavelha.model.Jogador;

public class JogadoresController {

    private Jogador jogador1;
    private Jogador jogador2;
    
    public JogadoresController() {
        this.jogador1 = new Jogador();
        this.jogador2 = new Jogador();        
        this.jogador1.setVitorias(0);
        this.jogador2.setVitorias(0);
    }
    
    public void setarSimboloJogador1(char s) {       
        this.jogador1.setSimbolo(s);       
    }
    
    public void setarSimboloJogador2(char s) {       
        this.jogador2.setSimbolo(s);
    }  

    public Jogador getJogador1() {
        return jogador1;
    }

    public void setJogador1(Jogador jogador1) {
        this.jogador1 = jogador1;
    }

    public Jogador getJogador2() {
        return jogador2;
    }

    public void setJogador2(Jogador jogador2) {
        this.jogador2 = jogador2;
    }
    
   
    
    
    
}
